FROM python:3.10

WORKDIR /cbgbot

COPY . .

RUN pip install -r requirements.txt

CMD python3 CBG.py