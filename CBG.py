from aiogram.utils import executor

from creat_cbg import dp
from database import cbg_db
from handlers import client, admin


async def on_startup(_):
	print('Бот работает')
	cbg_db.sql_start()


admin.Admin_Handlers_register(dp)

client.Client_Handlers_register(dp)


executor.start_polling(dp, skip_updates=True, on_startup=on_startup)
