from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher.filters import Text
from aiogram import types, Dispatcher
from creat_cbg import dp, bot
from aiogram.dispatcher.filters import Text
from database import cbg_db
from keyboards import admin_kb
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from random import randint
#Хочу стать админом
ID = None
admin = 0
chatid = 0
keyyyy = 0
async def admins(message: types.Message):
	global ID
	global chatid
	ID = message.from_user.id
	chatid *= 0
	chatid += message.chat.id
	await bot.send_message(message.from_user.id, "Ви відтепер адмін, але треба натиснути кнопку 'отримувати повідомлення'", reply_markup =admin_kb.button_case_admin)
	await message.delete()


class adminBoks(StatesGroup):
	photo = State()
	nameGry = State()
	opis = State()
	mesto = State()
	colvolud = State()
	price = State()
	ned = State()
	keey = State()









#
#
#
#
async def adminBoks_start(message: types.Message):
	if message.from_user.id == ID:
		await adminBoks.photo.set()
		await message.reply('Завантаж фото гри')


async def load_photo(message: types.Message, state: FSMContext):
	if message.from_user.id == ID:
		async with state.proxy() as data:
			data['photo'] = message.photo[0].file_id 
		await adminBoks.next()
		await message.reply('Вкажи назву гри')

async def load_nameGry(message: types.Message, state: FSMContext):

	if message.from_user.id == ID:
		async with state.proxy() as data:
			data['nameGry']= message.text
		await adminBoks.next()
		await message.reply('Вкажи день тижня, дату та час початку')

async def load_opis(message: types.Message, state: FSMContext):

	if message.from_user.id == ID:
		async with state.proxy() as data:
			data['opis']= message.text
		await adminBoks.next()
		await message.reply('Загальна кількість гравців')

async def load_mesto(message: types.Message, state: FSMContext):

	if message.from_user.id ==ID:
		async with state.proxy() as data:
			data['mesto']= message.text
		await adminBoks.next()

#
#async def load_colvolud(message: types.Message, state: FSMContext):
	if message.from_user.id == ID:
		async with state.proxy() as data:
			data['colvolud']= "@"+message.from_user.username
		await adminBoks.next()
		await message.reply('Інформація та коментарі')

async def load_price(message: types.Message, state: FSMContext):

	if message.from_user.id == ID:
		async with state.proxy() as data:
			data['price']= message.text
		await adminBoks.next()
		await message.reply('Вкажи порядковий номер')


async def nedel(message: types.Message, state: FSMContext):
	kek = randint(1000, 20000000)
	if message.from_user.id == ID:
		async with state.proxy() as data:
			data['ned']= message.text
		await adminBoks.next()




		async with state.proxy() as data:
			data['keey']= kek
		await cbg_db.sql_add_command(state)
		await message.reply('Партія зроблена')
		await state.finish()
		read = await cbg_db.sql_read2()
		b = str(kek)
		a = [b]
		for ret in read:
			if ret [7] == a[0]:
				await bot.send_photo(message.from_user.id, ret[0], f'{ret[1]}\nДата та час: {ret[2]},\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}', reply_markup=InlineKeyboardMarkup()\
					.add(InlineKeyboardButton(f'відправити івент у чат', callback_data=f'OTP {ret[7]}')).add(InlineKeyboardButton(f'Видалити', callback_data=f'del {ret[7]}')))

async def cancel_handler(message: types.Message, state: FSMContext):

	if message.from_user.id == ID:
		current_state = await state.get_state()
		if current_state is None:
			return
		await state.finish()
		await message.reply('Добре')


async def delete_box(message: types.Message):
	if message.from_user.id == ID:
		read = await cbg_db.sql_read2()
		for ret in read:
			await bot.send_photo(message.from_user.id, ret[0], f'{ret[1]}\nДата та час: {ret[2]},\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]},\nактуальність: {ret[6]} \nключ ивента: {ret[7]} ', reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'Видалити', callback_data=f'del {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити фото гри', callback_data=f'fot {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити назву', callback_data=f'naz {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Дату та час', callback_data=f'op {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити максимум людей', callback_data=f'Max {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Кто играет', callback_data=f'Col {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити актуальність', callback_data=f'Ned {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Коментар та правила', callback_data=f'Pri {ret[7]}'))\
				.add(InlineKeyboardButton(f'Відправити івент у чат', callback_data=f'OTP {ret[7]}')))
				

async def po_nazv(message: types.Message):
	read = await cbg_db.sql_read2()
	for ret in read:
		await bot.send_message(message.from_user.id, f'{ret[1]}\n {ret[2]}\n{ret[4]}', reply_markup=InlineKeyboardMarkup()\
			.add(InlineKeyboardButton(f'подивитись', callback_data=f'vsi {ret[7]}'))
			.add(InlineKeyboardButton(f'Видалити', callback_data=f'del {ret[7]}')))










@dp.callback_query_handler(lambda x: x.data and x.data.startswith('vsi '))
async def vsi_box(callback_query: types.CallbackQuery):
	read = await cbg_db.ludi(callback_query.data.replace('vsi ', ''))
	for ret in read:
		await callback_query.message.answer_photo(ret[0], f'{ret[1]}\nДата та час: {ret[2]},\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]},\nактуальність: {ret[6]} \nключ ивента: {ret[7]} ', reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'Видалити', callback_data=f'del {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити фото гри', callback_data=f'fot {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити назву', callback_data=f'naz {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Дату та час', callback_data=f'op {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити максимум людей', callback_data=f'Max {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Кто играет', callback_data=f'Col {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити актуальність', callback_data=f'Ned {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Коментар та правила', callback_data=f'Pri {ret[7]}'))\
				.add(InlineKeyboardButton(f'Відправити івент у чат', callback_data=f'OTP {ret[7]}')))
				












class redaktorf(StatesGroup):
	foto = State()
	keyf = State()
@dp.callback_query_handler(lambda x: x.data and x.data.startswith('fot '), state=None)
async def fot(callback_query: types.CallbackQuery):
	global keyyyy
	keyyyy = callback_query.data.replace('fot ', '')
	await redaktorf.foto.set()
	await callback_query.message.answer("Завантаж фото гри")
async def load_fot(message: types.Message, state: FSMContext):
	async with state.proxy() as lava:
			lava['foto']=  message.photo[0].file_id
	await redaktorf.next()
	async with state.proxy() as lava:
			lava['keey']= keyyyy
	await cbg_db.fot_command(state)
	await message.reply('редакція зроблена')
	await state.finish()
	read = await cbg_db.sql_read2()
	f = str(keyyyy)
	h =[f]
	for ret in read:
		if ret [7] == h[0]:
			await bot.send_photo(message.from_user.id, ret[0], f'{ret[1]}\nДата та час: {ret[2]},\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}', reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'Змінити фото гри', callback_data=f'fot {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити назву', callback_data=f'naz {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Дату та час', callback_data=f'op {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити максимум людей', callback_data=f'Max {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Кто играет', callback_data=f'Col {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити актуальність', callback_data=f'Ned {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Коментар та правила', callback_data=f'Pri {ret[7]}'))\
				.add(InlineKeyboardButton(f'Активувати івент', callback_data=f'Key {ret[7]}'))\
				.add(InlineKeyboardButton(f'відправити івент у чат', callback_data=f'OTP {ret[7]}'))\
				.add(InlineKeyboardButton(f'Видалити', callback_data=f'del {ret[7]}')))

















class redaktorN(StatesGroup):
	novinfaN = State()
	keyN = State()
@dp.callback_query_handler(lambda x: x.data and x.data.startswith('naz '), state=None)
async def naz(callback_query: types.CallbackQuery):
	global keyyyy
	keyyyy = callback_query.data.replace('naz ', '')
	await redaktorN.novinfaN.set()
	await callback_query.message.answer("Напиши нову назву")
async def load_nazN(message: types.Message, state: FSMContext):
	async with state.proxy() as lava:
			lava['novinfaN']= message.text
	await redaktorN.next()
	async with state.proxy() as lava:
			lava['keey']= keyyyy
	await cbg_db.naz_command(state)
	await message.reply('редакція зроблена')
	await state.finish()
	read = await cbg_db.sql_read2()
	f = str(keyyyy)
	h =[f]
	for ret in read:
		if ret [7] == h[0]:
			await bot.send_photo(message.from_user.id, ret[0], f'{ret[1]}\nДата та час: {ret[2]},\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}', reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'Змінити фото гри', callback_data=f'fot {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити назву', callback_data=f'naz {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Дату та час', callback_data=f'op {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити максимум людей', callback_data=f'Max {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Кто играет', callback_data=f'Col {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити актуальність', callback_data=f'Ned {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Коментар та правила', callback_data=f'Pri {ret[7]}'))\
				.add(InlineKeyboardButton(f'Активувати івент', callback_data=f'Key {ret[7]}'))\
				.add(InlineKeyboardButton(f'відправити івент у чат', callback_data=f'OTP {ret[7]}'))\
				.add(InlineKeyboardButton(f'Видалити', callback_data=f'del {ret[7]}')))



class redaktorO(StatesGroup):
	novinfaO = State()
	keyO = State()
@dp.callback_query_handler(lambda x: x.data and x.data.startswith('op '))
async def op(callback_query: types.CallbackQuery):
	global keyyyy
	keyyyy = callback_query.data.replace('op ', '')
	await redaktorO.novinfaO.set()
	await callback_query.message.answer('Напиши новy датy та час')
async def load_nazO(message: types.Message, state: FSMContext):
	async with state.proxy() as baba:
			baba['novinfaO']= message.text
	await redaktorO.next()
	async with state.proxy() as baba:
			baba['keey']= keyyyy
	await cbg_db.op_command(state)
	await message.reply('редакція зроблена')
	await state.finish()
	read = await cbg_db.sql_read2()
	f = str(keyyyy)
	h =[f]
	for ret in read:
		if ret [7] == h[0]:
			await bot.send_photo(message.from_user.id,ret[0], f'{ret[1]}\nДата та час: {ret[2]},\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}', reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'Змінити фото гри', callback_data=f'fot {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити назву', callback_data=f'naz {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Дату та час', callback_data=f'op {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити максимум людей', callback_data=f'Max {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Кто играет', callback_data=f'Col {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити актуальність', callback_data=f'Ned {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Коментар та правила', callback_data=f'Pri {ret[7]}'))\
				.add(InlineKeyboardButton(f'Активувати івент', callback_data=f'Key {ret[7]}'))\
				.add(InlineKeyboardButton(f'відправити івент у чат', callback_data=f'OTP {ret[7]}'))\
				.add(InlineKeyboardButton(f'Видалити', callback_data=f'del {ret[7]}')))


class redaktorK(StatesGroup):
	novinfaK = State()
	keyK = State()
@dp.callback_query_handler(lambda x: x.data and x.data.startswith('Col '))
async def cal(callback_query: types.CallbackQuery):
	global keyyyy
	keyyyy = callback_query.data.replace('Col ', '')
	await redaktorK.novinfaK.set()
	await callback_query.message.answer('Напиши другие ники')
async def load_nazK(message: types.Message, state: FSMContext):
	async with state.proxy() as vava:
			vava['novinfaK']= message.text
	await redaktorK.next()
	async with state.proxy() as vava:
			vava['keey']= keyyyy
	await cbg_db.cal_command(state)
	await message.reply('редакція зроблена')
	await state.finish()
	read = await cbg_db.sql_read2()
	f = str(keyyyy)
	h =[f]
	for ret in read:
		if ret [7] == h[0]:
			await bot.send_photo(message.from_user.id,ret[0], f'{ret[1]}\nДата та час: {ret[2]},\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}', reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'Змінити фото гри', callback_data=f'fot {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити назву', callback_data=f'naz {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Дату та час', callback_data=f'op {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити максимум людей', callback_data=f'Max {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Кто играет', callback_data=f'Col {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити актуальність', callback_data=f'Ned {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Коментар та правила', callback_data=f'Pri {ret[7]}'))\
				.add(InlineKeyboardButton(f'Активувати івент', callback_data=f'Key {ret[7]}'))\
				.add(InlineKeyboardButton(f'відправити івент у чат', callback_data=f'OTP {ret[7]}'))\
				.add(InlineKeyboardButton(f'Видалити', callback_data=f'del {ret[7]}')))



class redaktorP(StatesGroup):
	novinkom= State()
	keyP = State()
@dp.callback_query_handler(lambda x: x.data and x.data.startswith('Pri '))
async def comen(callback_query: types.CallbackQuery):
	global keyyyy
	keyyyy = callback_query.data.replace('Pri ', '')
	await redaktorP.novinkom.set()
	await callback_query.message.answer('Напиши новый коментарий')
async def com(message: types.Message, state: FSMContext):
	async with state.proxy() as vava:
			vava['novinkom']= message.text
	await redaktorP.next()
	async with state.proxy() as vava:
			vava['keey']= keyyyy
	await cbg_db.price_command(state)
	await message.reply('редакція зроблена')
	await state.finish()
	read = await cbg_db.sql_read2()
	f = str(keyyyy)
	h =[f]
	for ret in read:
		if ret [7] == h[0]:
			await bot.send_photo(message.from_user.id,ret[0], f'{ret[1]}\nДата та час: {ret[2]},\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}', reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'Змінити фото гри', callback_data=f'fot {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити назву', callback_data=f'naz {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Дату та час', callback_data=f'op {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити максимум людей', callback_data=f'Max {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Кто играет', callback_data=f'Col {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити актуальність', callback_data=f'Ned {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Коментар та правила', callback_data=f'Pri {ret[7]}'))\
				.add(InlineKeyboardButton(f'Активувати івент', callback_data=f'Key {ret[7]}'))\
				.add(InlineKeyboardButton(f'відправити івент у чат', callback_data=f'OTP {ret[7]}'))\
				.add(InlineKeyboardButton(f'Видалити', callback_data=f'del {ret[7]}')))



class redaktorM(StatesGroup):
	novinM= State()
	keyM = State()
@dp.callback_query_handler(lambda x: x.data and x.data.startswith('Max '))
async def max(callback_query: types.CallbackQuery):
	global keyyyy
	keyyyy = callback_query.data.replace('Max ', '')
	await redaktorM.novinM.set()
	await callback_query.message.answer('Напиши нову кількість')
async def maxx(message: types.Message, state: FSMContext):
	async with state.proxy() as vava:
			vava['novinkom']= message.text
	await redaktorP.next()
	async with state.proxy() as vava:
			vava['keey']= keyyyy
	await cbg_db.Mes_command(state)
	await message.reply('редакція зроблена')
	await state.finish()
	read = await cbg_db.sql_read2()
	f = str(keyyyy)
	h =[f]
	for ret in read:
		if ret [7] == h[0]:
			await bot.send_photo(message.from_user.id, ret[0], f'{ret[1]}\nДата та час: {ret[2]},\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}', reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'Змінити фото гри', callback_data=f'fot {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити назву', callback_data=f'naz {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Дату та час', callback_data=f'op {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити максимум людей', callback_data=f'Max {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Кто играет', callback_data=f'Col {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити актуальність', callback_data=f'Ned {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Коментар та правила', callback_data=f'Pri {ret[7]}'))\
				.add(InlineKeyboardButton(f'Активувати івент', callback_data=f'Key {ret[7]}'))\
				.add(InlineKeyboardButton(f'відправити івент у чат', callback_data=f'OTP {ret[7]}'))\
				.add(InlineKeyboardButton(f'Видалити', callback_data=f'del {ret[7]}')))

class redaktorNed(StatesGroup):
	Ned = State()
	NedNed = State()
@dp.callback_query_handler(lambda x: x.data and x.data.startswith('Ned '))
async def Need(callback_query: types.CallbackQuery):
	global keyyyy
	keyyyy = callback_query.data.replace('Ned ', '')
	await redaktorNed.Ned.set()
	await callback_query.message.answer('Напиши новую актуальность')
async def Neeed(message: types.Message, state: FSMContext):
	async with state.proxy() as vava:
			vava['Ned']= message.text
	await redaktorP.next()
	async with state.proxy() as vava:
			vava['NedNed']= keyyyy
	await cbg_db.ned_command(state)
	await message.reply('редакція зроблена')
	await state.finish() 
	read = await cbg_db.sql_read2()
	f = str(keyyyy)
	h =[f]
	for ret in read:
		if ret [7] == h[0]:
			await bot.send_photo(message.from_user.id, ret[0], f'{ret[1]}\nДата та час: {ret[2]},\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}', reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'Змінити фото гри', callback_data=f'fot {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити назву', callback_data=f'naz {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Дату та час', callback_data=f'op {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити максимум людей', callback_data=f'Max {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Кто играет', callback_data=f'Col {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити актуальність', callback_data=f'Ned {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Коментар та правила', callback_data=f'Pri {ret[7]}'))\
				.add(InlineKeyboardButton(f'Активувати івент', callback_data=f'Key {ret[7]}'))\
				.add(InlineKeyboardButton(f'відправити івент у чат', callback_data=f'OTP {ret[7]}'))\
				.add(InlineKeyboardButton(f'Видалити', callback_data=f'del {ret[7]}')))
class redaktorkey(StatesGroup):
	KeyKey = State()
	OldKey = State()
@dp.callback_query_handler(lambda x: x.data and x.data.startswith('Key '))
async def load_keyKey(callback_query: types.CallbackQuery, state: FSMContext):
	await callback_query.message.delete()
	await redaktorkey.KeyKey.set()
	async with state.proxy() as vava:
			vava['keey']= randint(1000, 20000000)
	await redaktorkey.next()
	async with state.proxy() as vava:
			vava['Oldkeey'] = callback_query.data.replace("Key ", '')
	await cbg_db.Key_command(state)
	await state.finish()
	await callback_query.answer('гра активна', show_alert=True)
	read = await cbg_db.sql_read2()
	for ret in read:
		if ret[7] == callback_query.data.replace('Key ', ''):
			await callback_query.message.answer_photo(ret[0], f'{ret[1]}\nДата та час: {ret[2]},\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}, Актуальність {ret[6]}', reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'Змінити фото гри', callback_data=f'fot {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити назву', callback_data=f'naz {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Дату та час', callback_data=f'op {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити максимум людей', callback_data=f'Max {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Кто играет', callback_data=f'Col {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити актуальність', callback_data=f'Ned {ret[7]}'))\
				.add(InlineKeyboardButton(f'Змінити Коментар та правила', callback_data=f'Pri {ret[7]}'))\
				.add(InlineKeyboardButton(f'Активувати івент', callback_data=f'Key {ret[7]}'))\
				.add(InlineKeyboardButton(f'відправити івент у чат', callback_data=f'OTP {ret[7]}'))\
				.add(InlineKeyboardButton(f'Видалити', callback_data=f'del {ret[7]}')))

@dp.callback_query_handler(lambda x: x.data and x.data.startswith('del '))
async def del_callback_run(callback_query: types.CallbackQuery):
	await callback_query.message.delete()
	await cbg_db.cbg_delete_command(callback_query.data.replace('del ', ''))
	await callback_query.answer(text=f'{callback_query.data.replace("del ", "")} удалена', show_alert=True)






async def oglaska(message: types.Message):
	await message.delete()
	if message.from_user.id == ID:
		read = await cbg_db.sql_read2()
		for ret in read:
			await bot.send_photo(message.from_user.id, ret[0], f'{ret[1]}\nДата та час: {ret[2]},\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]},\nАктуальність {ret[6]} ключ ивента: {ret[7]}', reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'відправити івент у чат', callback_data=f'OTP {ret[7]}')).add(InlineKeyboardButton(f'Видалити', callback_data=f'del {ret[7]}')))


async def loxi(message: types.Message):
	await message.delete()
	if message.from_user.id == ID:
		read = await cbg_db.sql_read2()
		for ret in read:
			if len(ret [7]) < 4:
				await bot.send_photo(message.from_user.id, ret[0], f'{ret[1]}\nДата та час :{ret[2]},\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]},\nАктуальність {ret[6]} ключ ивента: {ret[7]}', reply_markup=InlineKeyboardMarkup()\
					.add(InlineKeyboardButton(f'Активувати івент', callback_data=f'Key {ret[7]}')).add(InlineKeyboardButton(f'відправити івент у чат', callback_data=f'OTP {ret[7]}')).add(InlineKeyboardButton(f'Видалити', callback_data=f'del {ret[7]}')))



@dp.callback_query_handler(lambda x: x.data and x.data.startswith('OTP '))
async def otpravkaa(callback_query: types.CallbackQuery):
	read = await cbg_db.otp_command(callback_query.data.replace('OTP ', ''))
	for ret in read:
		await bot.send_photo(chatid, ret[0], f'{ret[1]}\nДата та час: {ret[2]},\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}',reply_markup=InlineKeyboardMarkup()\
		.add(InlineKeyboardButton(f'Зареєструватись', callback_data=f'register {ret[7]}')))










def Admin_Handlers_register(dp: Dispatcher):
	dp.register_message_handler(adminBoks_start, text =['Створити партію'], state=None)
	dp.register_message_handler(load_photo, content_types=['photo'], state=adminBoks.photo)
	dp.register_message_handler(load_nameGry, content_types =['text'], state = adminBoks.nameGry)
	dp.register_message_handler(load_opis,content_types =['text'],  state = adminBoks.opis)
	dp.register_message_handler(load_mesto,content_types =['text'],  state = adminBoks.mesto)
	dp.register_message_handler(load_price, content_types =['text'], state = adminBoks.price)
	dp.register_message_handler(nedel, state = adminBoks.ned)
	dp.register_message_handler(cancel_handler, state ='*', commands='отмена')
	dp.register_message_handler(cancel_handler,Text(equals='отмена', ignore_case=True), state="*" )
	dp.register_message_handler(admins, commands="mod", is_chat_admin=True)
	dp.register_message_handler(load_nazN, state = redaktorN.novinfaN)
	dp.register_message_handler(load_nazO, state = redaktorO.novinfaO)
	dp.register_message_handler(load_nazK, state = redaktorK.novinfaK)
	dp.register_message_handler(com, state = redaktorP.novinkom)
	dp.register_message_handler(maxx, state = redaktorM.novinM)
	dp.register_message_handler(Neeed, state = redaktorNed.Ned)
	dp.register_message_handler(load_fot,content_types=['photo'], state = redaktorf.foto)
	dp.register_message_handler(delete_box, text = ['подивитись усі партії'])
	dp.register_message_handler(oglaska, text = ['публікація партії у чаті'])
	dp.register_message_handler(loxi, text = ['Показати незатверджені'])
	dp.register_message_handler(po_nazv, text = ['Краткий варіант'])











