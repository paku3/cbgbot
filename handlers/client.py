from aiogram import types, Dispatcher
from creat_cbg import dp, bot
from keyboards import client_kb
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from database import cbg_db
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
import re 
from aiogram.dispatcher.filters import Text
from random import randint


knopmesto = InlineKeyboardMarkup(row_width=2)
knopka2 = InlineKeyboardButton(text = 'Ми в Instagram', url ='https://instagram.com/cbgames_kyiv?igshid=YTY2NzY3YTc=')
knopka4 = InlineKeyboardButton(text = 'Ми в Facebook', url ='https://www.facebook.com/CBGames.com.ua/')
knopka5 = InlineKeyboardButton(text = 'Колекція ігор', url ='https://www.cbgames.kiev.ua/uk/kollekciya-igr-kluba')
knopka6 = InlineKeyboardButton(text = 'Контакти', url ='https://www.cbgames.kiev.ua/uk/kontakty')
knopka7 = InlineKeyboardButton(text = 'Ціни Клубу', url ='https://www.cbgames.kiev.ua/uk/klubnyj-vznos-i-deyatelnost')
knopka8 = InlineKeyboardButton(text = 'Знижки', url ='https://www.cbgames.kiev.ua/uk/skidki-i-akcii')
knopka9 = InlineKeyboardButton(text = 'Чати Клубу', url ='https://www.cbgames.kiev.ua/chaty-kluba-cbgames-v-telegram')
knopka10= InlineKeyboardButton(text = 'Сайт магазину', url ='http://cbgames.com.ua/')
knopka12= InlineKeyboardButton(text = 'Сайт Клубу', url ='http://cbgames.kiev.ua/')
knopka11= InlineKeyboardButton(text = 'FAQ бот та інше', url ='https://www.cbgames.kiev.ua/uk/faq/faq-chavo.html')

knopmesto.add(knopka5, knopka6, knopka7, knopka8, knopka9, knopka2, knopka10, knopka12, knopka11)

knop = InlineKeyboardMarkup(row_width=2)
knop1 = InlineKeyboardButton(text = 'Оренда кімнат', url ='https://www.cbgames.kiev.ua/uk/arenda-komnat')
knop2 = InlineKeyboardButton(text = 'Оренда ігор', url ='https://www.cbgames.kiev.ua/uk/prokat-nastolnyx-igr-club-board-games')
knop3 = InlineKeyboardButton(text = 'Купівля ігор', url ='http://cbgames.com.ua/')
knop4 = InlineKeyboardButton(text = 'Trade in ігор', url ='https://www.cbgames.kiev.ua/uk/trade-in-obmin-nastilnix-igor')
knop.add(knop1, knop2, knop3, knop4)

knopk = InlineKeyboardMarkup(row_width=2)
knopk1 = InlineKeyboardButton(text = 'Ігротеки', url ='https://www.cbgames.kiev.ua/gde-poigrat-v-kieve-v-nastolnye-igry')
knopk2 = InlineKeyboardButton(text = 'Зустрічі з НРІ', url ='https://www.cbgames.kiev.ua/nastolnye-rolevye-igry-v-cbgames')
knopk3 = InlineKeyboardButton(text = 'Мафія', url ='https://www.cbgames.kiev.ua/gde-poigrat-v-kieve-v-mafiyu')
knopk4 = InlineKeyboardButton(text = 'Warhammer', url ='https://www.cbgames.kiev.ua/wargame-in-cbgames')
knopk5 = InlineKeyboardButton(text = 'Cash Flow', url ='https://www.cbgames.kiev.ua/trening-po-igre-krysinye-bega-cashflow')
knopk6 = InlineKeyboardButton(text = 'English Speaking Game Club', url ='https://www.cbgames.kiev.ua/sobitiya-cbg/english-speaking-game-club.html')
knopk7 = InlineKeyboardButton(text = 'Майстер-класи', url ='https://www.cbgames.kiev.ua/uk/master-klassy-po-sborke-i-pokraske-miniatyur-warhammer')
knopk8 = InlineKeyboardButton(text = 'Дитячі ігротеки', url ='https://www.cbgames.kiev.ua/uk/children-games')
knopk.add(knopk1, knopk2, knopk3, knopk4, knopk5, knopk6, knopk7, knopk8)


knopki = InlineKeyboardMarkup(row_width=2)
knopki1 = InlineKeyboardButton(text = 'Фарбування мініатюр', url ='https://cbgames.com.ua/ua/modelism.htm')
knopki2 = InlineKeyboardButton(text = 'Дайси на замовлення', url ='https://cbgames.com.ua/ua/lozadice.htm')
knopki3 = InlineKeyboardButton(text = 'Аксесуари НРІ виготовлення', url ='https://cbgames.com.ua/ua/dracoswood.htm')
knopki4 = InlineKeyboardButton(text = 'Корпоратив', url ='https://www.cbgames.kiev.ua/uk/provedenie-vecherinok-i-korporativov')
knopki5 = InlineKeyboardButton(text = '3D друк', url ='https://cbgames.com.ua/ua/3d-pechat-na-zakaz.htm')
knopki.add(knopki1, knopki2, knopki3, knopki4, knopki5)





global yvedf
yvedf = 0			

async def yved(message: types.Message):

	global yvedf
	yvedf *= 0
	yvedf += message.from_user.id
	await bot.send_message(yvedf, 'Ты стал админом')




class clientBoks(StatesGroup):
	photo = State()
	nameGry = State()
	opis = State()
	mesto = State()
	colvolud = State()
	price = State()
	ned = State()
	keey = State()








async def CliBoks_start(message: types.Message):
	await clientBoks.photo.set()
	await message.reply('Завантаж фото гри')


async def Cliload_photo(message: types.Message, state: FSMContext):
	async with state.proxy() as data:
		data['photo'] = message.photo[0].file_id 
	await clientBoks.next()
	await message.reply('Вкажи назву гри')

async def Cliload_nameGry(message: types.Message, state: FSMContext):
	async with state.proxy() as data:
		data['nameGry']= message.text
	await clientBoks.next()
	await message.reply('Вкажи день тижня, дату та час початку')

async def Cliload_opis(message: types.Message, state: FSMContext):
	async with state.proxy() as data:
		data['opis']= message.text
	await clientBoks.next()
	await message.reply('Загальна кількість гравців')

async def Cliload_mesto(message: types.Message, state: FSMContext):

	async with state.proxy() as data:
		data['mesto']= message.text
	await clientBoks.next()

	async with state.proxy() as data:
		if message.from_user.username == None:

			data['colvolud']= "+1"
		else:
			data['colvolud']= "@"+message.from_user.username
	await clientBoks.next()
	await message.reply('Інформація та коментарі (текстом)')

async def Cliload_price(message: types.Message, state: FSMContext):
	c = randint(1, 1000000000)
	async with state.proxy() as data:
		data['price']= message.text
	await clientBoks.next()

	async with state.proxy() as data:
		data['ned']= c 
	await clientBoks.next()

	async with state.proxy() as data:
		pep = randint(-99, 999)
		data['keey']= pep
	await cbg_db.sql_add_command(state)
	await state.finish()
	user = message.from_user.username
	if yvedf != 0:		
		read = await cbg_db.sql_read2()
		f = str(pep)
		h =[f]
		for ret in read:
			if ret [7] == h[0]:
				await bot.send_message(yvedf, '⚠️⚠️⚠️Кліент зробив нову гру⚠️⚠️⚠️:')
				await bot.send_photo(yvedf, ret[0], f'{ret[1]}\nДата та час: {ret[2]},\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}', reply_markup=InlineKeyboardMarkup()\
					.add(InlineKeyboardButton(f'Змінити фото гри', callback_data=f'fot {ret[7]}'))\
					.add(InlineKeyboardButton(f'Змінити назву', callback_data=f'naz {ret[7]}'))\
					.add(InlineKeyboardButton(f'Змінити Дату та час', callback_data=f'op {ret[7]}'))\
					.add(InlineKeyboardButton(f'Змінити максимум людей', callback_data=f'Max {ret[7]}'))\
					.add(InlineKeyboardButton(f'Змінити Кто играет', callback_data=f'Col {ret[7]}'))\
					.add(InlineKeyboardButton(f'Змінити актуальність', callback_data=f'Ned {ret[7]}'))\
					.add(InlineKeyboardButton(f'Змінити Коментар та правила', callback_data=f'Pri {ret[7]}'))\
					.add(InlineKeyboardButton(f'Активувати івент', callback_data=f'Key {ret[7]}'))\
					.add(InlineKeyboardButton(f'відправити івент у чат', callback_data=f'OTP {ret[7]}'))\
					.add(InlineKeyboardButton(f'Видалити', callback_data=f'del {ret[7]}')))
		await message.reply("Дякуємо, з вами зв'яжеться адміністратор")
	else:
		await message.reply('Надіслано адміну на затвердження')


async def Clicancel_handler(message: types.Message, state: FSMContext):
	current_state = await state.get_state()
	if current_state is None:
		return
	await state.finish()
	await message.reply('Ладно')



async def command_start(message : types.Message):
	await bot.send_message(message.from_user.id, 'Вітаю! Це бот Клуба CBGames.  Допоможу зібрати партію, забронювати місце, подивитися афішу тощо. Обери кнопкою в меню, що саме цікавить.  ', reply_markup= client_kb.kb_client)




knopmenu = InlineKeyboardMarkup(row_width=1)
knopkamenu = InlineKeyboardButton(text = 'Умови аренди кімнати', url ='https://www.cbgames.kiev.ua/uk/arenda-komnat')
knopmenu.add(knopkamenu)




async def menu(message : types.Message):
	await bot.send_message(message.from_user.id, 'Якщо бажаєте провести у Клубі захід (день народження, зустріч друзів тощо), щоб вас ніхто не турбував з інших гостей Клубу, то можна арендувати окрему кімнату.', reply_markup= knopmenu)

async def contacts(message: types.Message):
	await bot.send_message(message.from_user.id, "Наши посилання:", reply_markup=knopmesto)


knopmagaz = InlineKeyboardMarkup(row_width=1)
knopkamag = InlineKeyboardButton(text = 'Умови аренди кімнати', url ='https://cbgames.com.ua/')
knopmagaz.add(knopkamag)	
async def games(message: types.Message):
		await bot.send_message(message.from_user.id, "Оренда, купівля, обмін", reply_markup= knop)
		await bot.send_message(message.from_user.id, "Напрями роботи Клубу", reply_markup= knopk)
		await bot.send_message(message.from_user.id, "Інші не менш круті послуги", reply_markup= knopki)
	

async def Xohy_gratis(message: types.Message):
	await bot.send_message(message.from_user.id, "Можеш ознайомитися з запланованими партіямі й подіямі у Клубі та долучитися до них чи зробити свою", reply_markup = client_kb.KG)




class redaktorM(StatesGroup):
	novinfaM = State()
	keyM = State()




async def po_nazvi(message: types.Message):
	read = await cbg_db.sql_read2()
	for ret in read:
		if len(ret [7]) > 3:
			await bot.send_message(message.from_user.id, f'{ret[1]}', reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'Цікаво', callback_data=f'vibor {ret[1]}')))

@dp.callback_query_handler(lambda x: x.data and x.data.startswith('vibor '))
async def nazv_callback_run(callback_query: types.CallbackQuery):

	read = await cbg_db.cbg_nazva_command(callback_query.data.replace('vibor ', ''))
	for ret in read:
		if len(ret [7]) > 3:
			await callback_query.message.answer_photo(ret[0], f"{ret[1]}\nДата та час: {ret[2]}\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}",reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'Зареєструватись', callback_data=f'register {ret[7]}')))
	await callback_query.answer()


async def po_date(message: types.Message):

	read = await cbg_db.sql_read2()
	for ret in read:
		if len(ret [7]) > 3:
			await bot.send_message(message.from_user.id, f'{ret[1]}\n{ret[2]}', reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'Цікаво', callback_data=f'data {ret[7]}')))


@dp.callback_query_handler(lambda x: x.data and x.data.startswith('data '))
async def data_callback_run(callback_query: types.CallbackQuery):
	read = await cbg_db.ludi(callback_query.data.replace('data ', ''))
	for ret in read:
		await callback_query.message.answer_photo(ret[0], f"{ret[1]}\nДата та час: {ret[2]}\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}",reply_markup=InlineKeyboardMarkup()\
			.add(InlineKeyboardButton(f'Зареєструватись', callback_data=f'register {ret[7]}')))
	await callback_query.answer()



@dp.callback_query_handler(lambda x: x.data and x.data.startswith('colvolud '))

async def data_callback_run(callback_query: types.CallbackQuery):
	read = await cbg_db.cbg_colvolud_command(callback_query.data.replace('colvolud ', ''))
	for ret in read:
		if len(ret [7]) > 3:
			await callback_query.message.answer_photo(ret[0], f"{ret[1]}\nДата та час: {ret[2]}\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}",reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'Зареєструватись', callback_data=f'register {ret[7]}')))
	await callback_query.answer()
async def plevat_box(message: types.Message):
	read = await cbg_db.sql_read2()
	for ret in read:
		if len(ret [7]) > 3:
			await bot.send_photo(message.from_user.id, ret[0], f"{ret[1]}\nДата та час: {ret[2]}\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}",reply_markup=InlineKeyboardMarkup()\
			.	add(InlineKeyboardButton(f'Зареєструватись', callback_data=f'register {ret[7]}')))




@dp.callback_query_handler(lambda x: x.data and x.data.startswith('register '))
async def reg(callback_query: types.CallbackQuery, state: FSMContext):
	await callback_query.answer(text="Ви зареєструвались! (Бот може ще не встиг підтягнути ваш нік)", show_alert=True)
	await callback_query.message.delete()
	await redaktorM.novinfaM.set()
	read = await cbg_db.ludi(callback_query.data.replace('register ', ''))
	for ret in read:
		async with state.proxy() as vava:
				if callback_query.from_user.username == None:
					vava['colvolud']= ret[4]+",+1"
				else:
					vava['novinfaM'] = ret[4]+", @"+callback_query.from_user.username
	await redaktorM.next()	
	async with state.proxy() as vava:
			vava['keey']= callback_query.data.replace('register ', '')
	await cbg_db.cal_command(state)
	await state.finish()
	read = await cbg_db.sql_read2()
	for ret in read:
		if ret[7] == callback_query.data.replace('register ', ''):
			await callback_query.message.answer_photo(ret[0], f"{ret[1]}\nДата та час: {ret[2]}\nМаксимум людей: {ret[3]}\nХто грає: {ret[4]}\nІнформація та коментарі: {ret[5]}",reply_markup=InlineKeyboardMarkup()\
				.add(InlineKeyboardButton(f'Зареєструватись', callback_data=f'register {ret[7]}')))
			
			if yvedf == 0:
				await callback_query.answer("Адміністратор не на зв'язку")
			else:
				await bot.send_message(yvedf, f'Є реєстрація на гру під ключем {ret[7]}, актуальністю {ret[6]}, датою {ret[2]}, назвою {ret[1]} ')
	
async def svaz(message: types.Message):
	if message.from_user.username == None:
		await message.reply("У вас нема нікнейму по якому можно з вами зв'язатись")
	else:
		user = message.from_user.username
		if yvedf != 0:
			await bot.send_message(yvedf, "⚠️⚠️⚠️З вами хоче зв'язатись  @" + user)
			await message.reply("Адміністратор зв'яжеться")
		else:
			await message.reply("Адміністратор не на зв'язку")



def Client_Handlers_register(dp: Dispatcher):
	dp.register_message_handler(command_start, text = ['Назад в меню'])
	dp.register_message_handler(command_start, commands = ['start', 'help'])
	dp.register_message_handler(contacts, text = ['Інформація та контакти'])
	dp.register_message_handler(games, text = ['Всі послуги'])
	dp.register_message_handler(menu, text = ['Бронювання столів/кімнат'])
	dp.register_message_handler(Xohy_gratis, text = ['Подивитись заплановані  партії'])

	dp.register_message_handler(po_nazvi, text = ['Сортувати по назві'])
	dp.register_message_handler(po_date, text = ['Показати афішу партій і подій'])
	dp.register_message_handler(plevat_box, text = ['Показати розгорнуту інформацію'])

	dp.register_message_handler(svaz, text = ['Написати адміну'])

	dp.register_message_handler(CliBoks_start, text =['Зібрати партію'], state=None)
	dp.register_message_handler(Cliload_photo, content_types=['photo'], state=clientBoks.photo)
	dp.register_message_handler(Cliload_nameGry,content_types =['text'],  state = clientBoks.nameGry)
	dp.register_message_handler(Cliload_opis,content_types =['text'],  state = clientBoks.opis)
	dp.register_message_handler(Cliload_mesto,content_types =['text'],  state = clientBoks.mesto)
	dp.register_message_handler(Cliload_price, content_types =['text'], state = clientBoks.price)
	dp.register_message_handler(Clicancel_handler, state ='*', commands='отмена')
	dp.register_message_handler(Clicancel_handler,Text(equals='отмена', ignore_case=True), state="*" )

	dp.register_message_handler(yved, text = ['отримувати повідомлення'])
